﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour {
    
    private AudioSource _audioSource;
    public AudioClip[] audioClips;

    //    [SerializeField]
    //  private Animator anim;


    public void LoadAudioFiles() {
        audioClips = new AudioClip[SimulationSettings.totalSteps];

        for (int i=0; i<SimulationSettings.totalSteps; i++) {
            try {
                audioClips[i] = Resources.Load<AudioClip>("audio/STEP_" + i.ToString());//SimulationSettings.audioFilesDirectory + "STEP_" + i.ToString());
            } catch (Exception e) {
                Debug.Log(e.Message);
            }
        }
    }

    //public AudioManager() {
     //   _audioSource = GetComponent<AudioSource>();
      //  LoadAudioFiles();
    //}


    // Start is called before the first frame update
    void Start() {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.playOnAwake = false;
        _audioSource.loop = false;
    }

    // Update is called once per frame
    void Update() {
        
    }

    IEnumerator AudioInstruction(int index) {
        if(audioClips[index]) {
            _audioSource.PlayOneShot(audioClips[index], 0.9f);//can scale audio
            yield return new WaitForEndOfFrame();
            //If want to wait until end of the audioFile;
            //yield return new WaitForSeconds(audioClips[index].length);
        } else {
            Debug.Log("No more Clips");
            yield return new WaitForEndOfFrame();
        }
    }

    public void PlayStepAudio(int step) {
        _audioSource.clip = audioClips[step];
        if(SimulationSettings.currentStep < audioClips.Length){// && !_audioSource.isPlaying) {
            if (_audioSource.isPlaying)
                _audioSource.Stop();
            //_audioSource.clip = audioClips[step];
            //_audioSource.Play();
            //only if want to play two clips simulatanelously
            //_audioSource.PlayOneShot(audioClips[step], 0.9f);
            StartCoroutine(AudioInstruction(step));
        } else {
            Debug.Log("Can't Do that, audio playing or no more clips");
        }
    }


}
