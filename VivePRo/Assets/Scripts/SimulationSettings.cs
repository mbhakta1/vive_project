﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SimulationSettings  {

    public static string audioFilesDirectory;
    public static string animationFilesDirectory;

    public static int currentStep;
    public static int totalSteps;

    public static void loadSettings(string dir) {
        // TO DO: Add the load settings from drive based upon where you are if none then simply use these...

        currentStep = 0;
        totalSteps = 17;

    }

}
