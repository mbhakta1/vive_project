﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatStep : MonoBehaviour {

    public SimulationEngine simulationEngine;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickRepeat()
    {
        simulationEngine.RepeatStep();
    }
}
