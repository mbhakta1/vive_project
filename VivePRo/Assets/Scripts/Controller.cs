﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Controller : MonoBehaviour {

    public Hand hand;
    private EVRButtonId trigger = EVRButtonId.k_EButton_SteamVR_Trigger;
    public EVRButtonId grip = EVRButtonId.k_EButton_Grip;

    private SimulationEngine simulationEngine;

    GameObject equipObj;

    private void Start() {
        hand = GetComponent<Hand>();
        simulationEngine = GameObject.FindGameObjectWithTag("SimulationEngine").GetComponent<SimulationEngine>();
    }

    private void Update() {
        if(hand.controller.GetPressDown(trigger) && equipObj != null) {
            Debug.Log("Triggred Pressed");
            equipObj.transform.parent = transform;
            equipObj.GetComponent<Rigidbody>().isKinematic = true;
        }

        if(hand.controller.GetPressUp(trigger) && equipObj != null) {
            Debug.Log("trigger released, object nulled");
            equipObj.GetComponent<Rigidbody>().isKinematic = false;
            equipObj.transform.parent = null;
            equipObj = null;
        }

        if(hand.controller.GetPressUp(grip)) {
            Debug.Log("Audio should Change");
            simulationEngine.NextStep();
        }
    }

    private void OnTriggerEnter(Collider other) {
        Debug.Log("Trigger entered");
        if(equipObj == null) {
            equipObj = other.gameObject;
            Debug.Log(other.name);
        } else {
            Debug.Log("Not triggered");
        }
    }

    private void OnTriggerExit(Collider other) {
        Debug.Log("Trigger exit");
        if(equipObj != null) {
           // equipObj = null;
        }
    }
    /*
    protected SteamVR_TrackedObject trackedObj;
    private SteamVR_TrackedController trackedController;
    //public SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    private Hand hand;
    //Trigger button
    private EVRButtonId grip = EVRButtonId.k_EButton_SteamVR_Trigger;
    public EVRButtonId test = EVRButtonId.k_EButton_Grip;
    private bool testPressed = false;

    GameObject equipObj;

    public bool gripUp = false;
    public bool gripDown = true;
    

    private void Awake() {
        trackedController = GetComponent<SteamVR_TrackedController>();
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        hand = GetComponent<Hand>();

        trackedController.TriggerClicked += Trigger;
        //controler = GetComponent<hand>
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        /*
        gripDown = controller.GetPressDown(grip);
        gripUp = controller.GetPressUp(grip);
        testPressed = controller.GetPress(test);
        if(gripDown)
            Debug.Log(gripDown);

        if(gripDown && equipObj != null) {
            Debug.Log("Triggred Pressed");
            equipObj.transform.parent = transform;
            equipObj.GetComponent<Rigidbody>().isKinematic = true;
        }

        if(gripUp && equipObj != null) {
            Debug.Log("trigger released, object nulled");
            equipObj.GetComponent<Rigidbody>().isKinematic = false;
            equipObj.transform.parent = null;
            equipObj = null;
        }
        if(testPressed) {
            Debug.Log("Audio should Change");
           
        }
        
}

    void Trigger(object sender, ClickedEventArgs e) {
        Debug.Log("Trigger Pressed:");
    }

    private void OnTriggerEnter(Collider other) {
        if(equipObj == null) {
            equipObj = other.gameObject;
            Debug.Log(other.name);
        } else {
            Debug.Log("Not triggered");
        }
    }

    private void OnTriggerExit(Collider other) {
        Debug.Log("Trigger exit");
        if(equipObj != null) {
            equipObj = null;
        }
    }


    //public SteamVR_Action_Bollean grabAction = null;
    
    private Hand hand;
    private GameObject simulationEngine;


	// Use this for initialization
	void Start () {
        hand = GetComponent<Hand>();
        simulationEngine = GameObject.Find("SimulationEngine");
	}


    // Update is called once per frame
    void Update () {
        if(hand.controller.GetHairTriggerDown()) {
            simulationEngine.GetComponent<SimulationEngine>().NextStep();
            Debug.Log("============================================");
        }
    }
    */
}
