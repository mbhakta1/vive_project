﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour {

    private Animator _anim;
    //option to make the implementation more general purpose
    [SerializeField]
    private List<AnimationClip> _aClip = new List<AnimationClip>();
    private AnimatorClipInfo[] currClip;
    // Use this for initialization
    void Start () {
        _anim = GameObject.FindGameObjectWithTag("VirtualEnvironment").GetComponent<Animator>();
        _anim.enabled = true;
        //Loading all the clips
        //foreach (AnimationClip animClip in _anim.runtimeAnimatorController.animationClips)
        //{
        //    _aClip.Add(animClip);
        //}
        currClip = this._anim.GetCurrentAnimatorClipInfo(0);
    }
	
	// Update is called once per frame
	void Update () {
  //      if(Input.anyKeyDown) {
    //        Debug.Log("Key Pressed");
    //        _anim.SetInteger("step", 1);
    //        Debug.Log(_anim.GetInteger("step"));
     //   }
		
	}

    public void PlayStepAnimation(int step) {
        Debug.Log("ANIMATION Play Step Animation Step " + step + ", clip " + _aClip[step-1].name);

        _anim.SetInteger("step", step-1);
        _anim.SetInteger("step", step);
        currClip = this._anim.GetCurrentAnimatorClipInfo(0);

        //if (step >= 10)
        //{
        //    _anim.Play(_aClip[step].name, -1, 0f);
        //    _anim.speed = -1f;
        //}
        //else
        //{
        //    //_anim.SetInteger("step", step);
        //    _anim.Play(_aClip[step].name, -1, 0f);
        //}


        //_anim.SetInteger("step", step);

    }

    public void Replay()
    {
        _anim.Play(currClip[0].clip.name, -1, 0);
    }


}
