﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(SimulationEngine))]
[RequireComponent(typeof(AudioManager))]
[RequireComponent(typeof(AnimationManager))]
public class SimulationEngine : MonoBehaviour {

    [SerializeField]
    private AudioManager _audioManager;
    [SerializeField]
    private AnimationManager _animationManager;


    // Use this for initialization
    void Start () {
        SimulationSettings.loadSettings("");
        _audioManager = GetComponent<AudioManager>();
        _animationManager = GetComponent<AnimationManager>();
        Debug.Log("Current Dir "+System.IO.Directory.GetCurrentDirectory());
        _audioManager.LoadAudioFiles();
        _audioManager.PlayStepAudio(0);
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.F)) {
            Debug.Log("Key Pressed simulation engine");
            NextStep();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            PreviousStep();
        }
	}

    public int NextStep() {
        
        
        if(SimulationSettings.currentStep <= SimulationSettings.totalSteps)
        {
            //TODO Call the next step: 
            Debug.Log("Current Step: " + SimulationSettings.currentStep);
            SimulationSettings.currentStep++;
            _audioManager.PlayStepAudio(SimulationSettings.currentStep);
            _animationManager.PlayStepAnimation(SimulationSettings.currentStep);
            Debug.Log("NEXT Step: " + SimulationSettings.currentStep);
            return SimulationSettings.currentStep;
        }
        else
        {
            //Start from beginning or end the after pressing 3 times next button. 
            Debug.Log("Either End the Script or start from beginning.");
            return SimulationSettings.currentStep;
        }
    }

    public int PreviousStep()
    {
        
        if (SimulationSettings.currentStep > 0 && SimulationSettings.currentStep <= SimulationSettings.totalSteps)
        {
            //TODO Call the next step: 
            Debug.Log("Current Step: " + SimulationSettings.currentStep);
            SimulationSettings.currentStep--;
            _audioManager.PlayStepAudio(SimulationSettings.currentStep);
            _animationManager.PlayStepAnimation(SimulationSettings.currentStep);
            Debug.Log("NEXT Step: " + SimulationSettings.currentStep);
            return SimulationSettings.currentStep;
        }
        else
        {
            //Start from beginning or end the after pressing 3 times next button. 
            Debug.Log("Either End the Script or start from beginning.");
            return SimulationSettings.currentStep;
        }
    }

    public void RepeatStep()
    {
        Debug.Log("repeat");
        _audioManager.PlayStepAudio(SimulationSettings.currentStep);
        _animationManager.Replay();
    }


}
